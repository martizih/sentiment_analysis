import numpy as np
import csv
import random
from tensorflow.python.data.ops import dataset_ops
from tensorflow.python.keras.preprocessing import dataset_utils
from tensorflow.python.ops import io_ops
import tensorflow as tf
AUTOTUNE = tf.data.experimental.AUTOTUNE

def to_dataset(
    data,
    labels,
    class_names,
    batch_size=32,
    shuffle=True,
    seed=np.random.randint(1e6)
):
    data_ds = dataset_ops.Dataset.from_tensor_slices(data)
    label_ds = dataset_utils.labels_to_dataset(labels, "int", len(class_names))
    dataset = dataset_ops.Dataset.zip((data_ds, label_ds))
    if shuffle:
        dataset = dataset.shuffle(buffer_size=batch_size * 8, seed=seed)
    dataset = dataset.batch(batch_size)
    dataset.class_names = class_names
    dataset = dataset.cache().prefetch(buffer_size=AUTOTUNE)
    return dataset

def to_train_val_ds(
    data,
    labels,
    class_names,
    batch_size=32,
    shuffle=True,
    seed=np.random.randint(1e6),
    validation_split=None
):
    dataset_utils.check_validation_split_arg(validation_split, 'training', shuffle, seed)
    dataset_utils.check_validation_split_arg(validation_split, 'validation', shuffle, seed)
    train, label_train = dataset_utils.get_training_or_validation_split(data, labels, validation_split, 'training')
    val, label_val = dataset_utils.get_training_or_validation_split(data, labels, validation_split, 'validation')
    train_ds = to_dataset(
        train,
        label_train,
        class_names,
        batch_size=batch_size,
        shuffle=shuffle,
        seed=seed
    )
    val_ds = to_dataset(
        val,
        label_val,
        class_names,
        batch_size=batch_size,
        shuffle=shuffle,
        seed=seed
    )
    return train_ds, val_ds

def from_directory(
    directory,
    shuffle=True,
    seed=np.random.randint(1e6)
):
    data, labels, class_names = dataset_utils.index_directory(
        directory,
        labels='inferred',
        formats=('.txt',),
        class_names=None,
        shuffle=shuffle,
        seed=seed,
        follow_links=False
    )
    data = [io_ops.read_file(x) for x in data]
    return data, labels, class_names


def load_csv(path, header, encoding="utf-8"):
    with open(path, 'r', encoding=encoding) as fh:
        reader = csv.reader(fh, delimiter=',')
        return [{k:v for k,v in zip(header, line)} for line in reader]

def from_csv(
    path,
    shuffle=True,
    header=[],
    encoding="utf-8",
    convert_data=None,
    class_names=None,
    seed=np.random.randint(1e6)
):
    data = load_csv(path, header=header, encoding=encoding)
    if shuffle:
        random.Random(seed).shuffle(data)
    data, labels = convert_data(data)
    if not class_names:
        class_names = list(set(labels))
    labels = [class_names.index(x) for x in labels]
    return data, labels, class_names