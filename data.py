import tensorflow as tf
import os
import shutil
import dataset_utils
import random
import os

def download_data():
    dataset = tf.keras.utils.get_file(
        'aclImdb_v1.tar.gz',
        'https://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz',
        untar=True, 
        cache_dir='.',
        cache_subdir=''
    )
    dataset_dir = os.path.join(os.path.dirname(dataset), 'aclImdb')
    train_dir = os.path.join(dataset_dir, 'train')
    # remove unused folders to make it easier to load the data
    remove_dir = os.path.join(train_dir, 'unsup')
    shutil.rmtree(remove_dir)

def download_data2():
    tf.keras.utils.get_file(
        "archive.zip",
        origin="https://www.kaggle.com/kazanova/sentiment140/download",
        extract=True,
        archive_format='zip',
        cache_dir='.',
        cache_subdir=''
    )

def to_sentiment3(x):
    if x == '1':
        return 'neg'
    elif x == '2':
        return 'pos'
    else:
        raise ValueError("only negative and positive supported")

def convert_data3(data):
    texts = [x['text'] for x in data]
    labels = [to_sentiment3(x['sentiment']) for x in data]
    return texts, labels

def instantiate_data3(batch_size=32, seed=42):
    dirname = os.path.dirname(__file__)
    data, labels, class_names = dataset_utils.from_csv(
        os.path.join(dirname, "amazon_review_polarity_csv", "train.csv"),
        header=["sentiment", "title", "text"],
        convert_data=convert_data3,
        seed=seed
    )
    _data, _labels, _class_names = dataset_utils.from_csv(
        os.path.join(dirname, "amazon_review_polarity_csv", "test.csv"),
        header=["sentiment", "title", "text"],
        convert_data=convert_data3,
        seed=seed
    )
    train_ds, val_ds = dataset_utils.to_train_val_ds(
        data,
        labels,
        class_names,
        batch_size=batch_size,
        seed=seed,
        validation_split=0.2
    )
    test_ds = dataset_utils.to_dataset(
        data,
        labels,
        class_names,
        batch_size=batch_size,
        seed=seed
    )
    return train_ds, val_ds, test_ds

def shuffled(data, seed):
    x = data.copy()
    random.Random(seed).shuffle(x)
    return x

def instantiate_complete_data3(batch_size=32, seed=42):
    dirname = os.path.dirname(__file__)
    data, labels, class_names = dataset_utils.from_csv(
        os.path.join(dirname, "amazon_review_polarity_csv", "train.csv"),
        header=["sentiment", "title", "text"],
        convert_data=convert_data3,
        class_names=["neg", "pos"],
        seed=seed
    )
    _data, _labels, _class_names = dataset_utils.from_csv(
        os.path.join(dirname, "amazon_review_polarity_csv", "test.csv"),
        header=["sentiment", "title", "text"],
        convert_data=convert_data3,
        class_names=["neg", "pos"],
        seed=seed
    )
    train_ds, val_ds = dataset_utils.to_train_val_ds(
        shuffled(list(data) + list(_data), seed=seed),
        shuffled(list(labels) + list(_labels), seed=seed),
        list(set(class_names + _class_names)),
        batch_size=batch_size,
        seed=seed,
        validation_split=0.01
    )
    return train_ds, val_ds, None

def to_sentiment(x):
    if x == '0':
        return 'neg'
    elif x == '4':
        return 'pos'
    else:
        raise ValueError("only negative and positive supported")

def convert_data(data):
    texts = [x['text'] for x in data]
    labels = [to_sentiment(x['sentiment']) for x in data]
    return texts, labels

def instantiate_data2(batch_size=32, seed=42):
    data, labels, class_names = dataset_utils.from_csv(
        "training.1600000.processed.noemoticon.csv",
        header=["sentiment", "id", "data", "_", "name", "text"],
        encoding="latin-1",
        convert_data=convert_data,
        seed=seed
    )
    return dataset_utils.to_train_val_ds(
        data,
        labels,
        class_names,
        batch_size=batch_size,
        seed=seed,
        validation_split=0.2
    )
    

def instantiate_data(batch_size=32, seed=42):
    data, labels, class_names = dataset_utils.from_directory(
        'aclImdb/train',
        seed=seed
    )
    train_ds, val_ds = dataset_utils.to_train_val_ds(
        data,
        labels,
        class_names,
        batch_size=batch_size,
        seed=seed,
        validation_split=0.2
    )
    _data, _labels, _class_names = dataset_utils.from_directory(
        'aclImdb/test',
        seed=seed
    )
    test_ds = dataset_utils.to_dataset(
        _data,
        _labels,
        _class_names,
        batch_size=batch_size,
        seed=seed
    )
    return train_ds, val_ds, test_ds

def instantiate_complete_data(batch_size=32, seed=42):
    data, labels, class_names = dataset_utils.from_directory(
        'aclImdb/train',
        seed=seed
    )
    _data, _labels, _class_names = dataset_utils.from_directory(
        'aclImdb/test',
        seed=seed
    )
    return dataset_utils.to_train_val_ds(
        shuffled(list(data) + list(_data), seed=seed),
        shuffled(list(labels) + list(_labels), seed=seed),
        list(set(class_names + _class_names)),
        batch_size=batch_size,
        seed=seed,
        validation_split=0.2
    )