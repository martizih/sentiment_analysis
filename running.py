import os
import tensorflow as tf
import model as m
import data as d
import csv

def load_csv(path):
    dict_list = []
    with open(path, 'r') as fh:
        reader = csv.DictReader(fh,delimiter='\t')
        [dict_list.append(l) for l in reader]
    dict_list = [{key : value.strip()
        for key, value in d.items() if key}
            for d in dict_list]
    return dict_list



checkpoint_path = "/content/drive/MyDrive/large_result/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)


data = load_csv("example.csv")
data = [x['Hit Sentence'] for x in data]
data = list(set(data))

model = m.build_model(bert_model_name="bert_en_uncased_L-12_H-768_A-12")
model.load_weights(checkpoint_path)
model.compile()
result = model.predict(data)
result = [x[0] - 0.5 for x in result]
data = list(zip(result, data))
data = sorted(data, key=lambda x: x[0])
print('\n'.join(f"{x[0]:+.2f}: {x[1]}" for x in data[:50] + data[-50:]))