import os
import tensorflow as tf
import model as m
import data as d

train_epoch_minutes = 40
val_epoch_minutes = 5
epochs = 1000
batch_size = 32

def steps_per_minute():
    return int(72 * 32 / batch_size)

checkpoint_path = "/content/drive/MyDrive/large/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
checkpoint = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path,
    save_weights_only=True,
    save_best_only=True,
    monitor=f'val_{m.get_metrics().name}',
    verbose=1
)
earlystop = tf.keras.callbacks.EarlyStopping(
    monitor=f'val_{m.get_metrics().name}',
    patience=2
)


train_ds, val_ds, test_ds = d.instantiate_complete_data3(batch_size=batch_size, seed=42)

model = m.build_model(bert_model_name="bert_en_uncased_L-12_H-768_A-12")
model.load_weights(checkpoint_path)
model.summary()
model.compile(
    optimizer=m.get_optimizer(train_ds, epochs=epochs, init_lr=3e-5, warmup_steps=0.01),
    loss=m.get_loss(),
    metrics=m.get_metrics()
)

history = model.fit(
    x=train_ds.repeat(),
    validation_data=val_ds.repeat(),
    batch_size=batch_size,
    epochs=epochs,
    steps_per_epoch=train_epoch_minutes * steps_per_minute(),
    validation_steps=val_epoch_minutes * steps_per_minute(),
    callbacks=[checkpoint, earlystop]
)

model.evaluate(test_ds)