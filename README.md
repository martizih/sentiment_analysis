# Sentiment Analysis

A cutting-edge sentiment analysis model that achieves 95% binary accuracy over the amazon customer reviews dataset.

## Dataset

The Amazon reviews dataset consists of reviews from amazon. The data span a period of 18 years, including ~35 million reviews up to March 2013. Reviews include product and user information, ratings, and a plaintext review. For more information, please refer to the following paper | J. McAuley and J. Leskovec. Hidden factors and hidden topics | understanding rating dimensions with review text. RecSys, 2013.

The Amazon reviews polarity dataset is constructed by Xiang Zhang (xiang.zhang@nyu.edu) from the above dataset. It is used as a text classification benchmark in the following paper | Xiang Zhang, Junbo Zhao, Yann LeCun. Character-level Convolutional Networks for Text Classification. Advances in Neural Information Processing Systems 28 (NIPS 2015).

The Amazon reviews polarity dataset is constructed by taking review score 1 and 2 as negative, and 4 and 5 as positive. Samples of score 3 is ignored. In the dataset, class 1 is the negative and class 2 is the positive. Each class has 1,800,000 training samples and 200,000 testing samples.

## Model

The underlying model is a pretrained Bert model, which gives the model a basic understanding of the English language. This model is then retrained on the Amazon reviews polarity database, resulting in a sentiment classifier that can actually work on the whole sentence and not just single words.

## Example

This shows the power of the sentiment analysis network. The example data is a completely unrelated set of news articles that matched the search Tesla. 

Sentiment | Text
--------- | -----
-5.25 | Bad quality and service? Taxi company demands compensation from Tesla
-5.25 | Own too many Baillie Gifford funds (and too much Tesla)? Buy these instead
-5.22 | Tesla Halts Model S And X Production For More Than Two Weeks
-5.22 | Active Funds Ill-Prepared for Tesla Rebalance
-5.21 | Tesla stops Model S and Model X production for over two weeks
-5.19 | Elon Musk's attempt to take Tesla private in 2018 was the worst decision he ever made
-5.13 | Demand for expensive electric vehicles falls, Tesla to suspend production of Model S and Model X
-5.08 | Tesla suspends production of the S and X models for 18 days
-5.07 | Tesla Phone Screen Cache Problem
-5.05 | Forget NIO and Tesla. I’d rather buy and hold these cheap shares
-5.04 | Tesla will halt Model S and X production for 18 days
-5.00 | Tesla stock falls after $5 billion stock offering plan
-4.94 | Tesla Stock – Tesla Stock Baffling Valuation Makes It Uninvestable At This Level
-4.93 | Adding Tesla To The S&P 500 Was A Bad Idea In September, Even Worse Now
-4.93 | This Tesla Driver Made the Mistake of Sleeping Behind The Wheel, Now He’s Facing Charges
-4.92 | Tesla temporarily halts production on Models S and X
-4.91 | Tesla Stops Production of the Model S and Model X
-4.90 | About To Invest In Tesla? Consider This Battery Stock Instead
-4.90 | About to invest in Tesla? Consider this battery stock instead
-4.90 | Tesla forced to stop Berlin construction over unpaid 100 million euros
...
+3.67 | Tesla Stock Is Rising Ahead of Inclusion in the S&P 500. What to Watch This Week.
+3.76 | The Tell A huge stake in Tesla combined with a timely short bet have delivered massive gains for this Tiger cub
+3.82 | CEREBRUM-7T Fast and Fully-volumetric Brain Segmentation of 7 Tesla MR Volumes
+3.83 | Plug and Charge Brings Tesla-Style EV Charging Simplicity to Other Brands
+3.84 | Tesla S&P Debut Comes All at Once
+3.85 | Navy Seal Michael Jaco Nikola Tesla's Discoveries Are Here With This 5D EMF Protection Technology! - Great Video
+3.85 | Tesla LMPE 2030 hypercar promises high octane motorsports for race fans
+3.87 | The Tesla Christmas Mode is Guaranteed to Make You Feel Holly and Jolly
+3.89 | The Fake Holographic & Asteroids Preparing For The Day Of 1000 Stars And Asteroids. The Tesla Fireball Technology Is Real - Fabulous Videos!
+3.89 | Diving deep on Tesla and Zoom, plus wisdom from world's best stock-picker
+3.90 | Tesla Cybertruck, an electric pickup truck, wows crowd at unveiling
+3.92 | Gearing Up Tesla's Path To The S&P 500 Index Has Been A Wild Ride
+3.96 | Weekly Wrap Vanguard Hits a Milestone, Tesla Joins the S&P, and More
+3.96 | A huge stake in Tesla combined with a timely short bet have delivered massive gains for this 'Tiger cub'
+4.00 | The Dream of Apple CarPlay in a Tesla Comes True with Simple Yet Clever Setup
+4.03 | Tesla FSD beta's behind the scenes look reveals insane range of options and detail
+4.03 | Business Maverick Tesla S&P Debut to Come All at Once, Rippling Across Markets
+4.17 | thriving impulsive hike with key players  BMW, Gm Motors, Ford Motors, Tesla,Jlr, Denso Corporation, Airbiquity Inc.,Volvo Car Corporation,
+4.31 | VIDEO LENO On ELON And Tesla. And A Great FEEL GOOD Story Of A Car He Bought.
+5.10 | Designed by former SpaceX, Tesla, & AirBnB employees, this flatpack shelter is the ultimate adventure essential!

